package com.nordicsemi.nrfUARTv2;

import android.app.IntentService;
import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.SystemClock;
import android.content.Intent;
import android.util.Log;

public class SimpleIntentService extends IntentService {
    public static final String PARAM_IN_MSG = "imsg";
    public static final String PARAM_OUT_MSG = "omsg";
    public static int timeout = 5000;
    UartService mService = null;

    public SimpleIntentService() {
        super("SimpleIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        String msg = intent.getStringExtra(PARAM_IN_MSG);
        //SystemClock.sleep(30000); // 30 seconds
        String resultTxt = msg + " ";
        Log.i("praboo" , "Msg: " + msg);

        //Intent bindIntent = new Intent(this, UartService.class);
        //bindService(bindIntent, mServiceConnection, Context.BIND_AUTO_CREATE);



        byte[] value = null;
        int i = 0;
        while(true){
            Log.i("iii", "run:" + i);
            String message = "getdata";
            try{
                value = message.getBytes("UTF-8");
            }catch(Exception ex){
                ex.printStackTrace();
            }
            if(UartService.mService != null){
                Log.i("praboo", "Before call:" + i);
                try{
                    UartService.mService.writeRXCharacteristic(value);
                } catch(Exception ex){
                    //ignore
                }

            }

            SystemClock.sleep(timeout);
            i++;
        }

    }

    /*private ServiceConnection mServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder rawBinder) {
            mService = ((UartService.LocalBinder) rawBinder).getService();
            Log.d("kkk", "onServiceConnected mService= " + mService);
            if (!mService.initialize()) {
                Log.e("ll", "Unable to initialize Bluetooth");
                //finish();
            }

        }

        public void onServiceDisconnected(ComponentName classname) {
            ////     mService.disconnect(mDevice);
            mService = null;
        }
    };*/
}

